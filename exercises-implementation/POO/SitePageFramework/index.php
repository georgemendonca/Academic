<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// Incluindo as classes
include "class/Site.php";
include "class/Page.php";

// Configurando a página
$pg = new Page();
$pg->__set('tituloCab','Site dinâmico em PHP');
$pg->__set('charset','UTF-8');
$pg->__set('css',array('css/style.css'));
$pg->__set('tituloSite','Dinamizando um template HTML do http://www.w3schools.com');
$pg->__set('txtRodape', 'Copyright © 4° Semestre TADS');

// Montando a página
Site::cabecalho($pg);
Site::corpo($pg);
Site::rodape($pg);