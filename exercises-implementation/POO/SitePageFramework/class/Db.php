<?php
/**
 * Classe para conexão com banco de dados
 * @createDate 12/05/2015
 * @author George Mendonça
 * Disciplina de Programação Orientada a Objetos (POO)
 */
class Db extends PDO {

    public $isConnected;
    protected $objDb;
    
	/**
	 * Método construtor da classe
	 * @param STRING $username - Usuário do banco de dados
	 * @param STRING $password - Senha do usuário
	 * @param STRING $host - Servidor do banco de dados
	 * @param STRING $dbname - Nome do banco de dados
	 * @param STRING $options - Opções de configuração do banco de dados
	 */
    public function __construct($username, $password, $host, $dbname, $options=array())
    {
        $this->isConnected = true;
        try 
        { 
            $this->objDb = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options); 
            $this->objDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
            $this->objDb->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } 
        catch(PDOException $e) 
        { 
            $this->isConnected = false;
            throw new Exception($e->getMessage());
        }
    }
    /**
     * Método para disconectar o banco de dados
     */
    public function Disconnect()
    {
        $this->objDb = null;
        $this->isConnected = false;
    }
    
	/**
	 * Método para Recuperar um registro do banco de dados
	 * @param STRING $query - Consulta
	 * @param STRING $params - Parâmetros do filtro
	 */
    public function getRow($query, $params=array())
    {
        try
        { 
            $stmt = $this->objDb->prepare($query); 
            $stmt->execute($params);
            return $stmt->fetch();  
        }catch(PDOException $e)
        {
            throw new Exception($e->getMessage());
        }
    }
    
	/**
	 * Método para recuperar registros do banco de dados
	 * @param STRING $query - Consulta
	 * @param STRING $params - Parâmetros do filtro
	 */
    public function getRows($query, $params=array()){
        try
        { 
            $stmt = $this->objDb->prepare($query); 
            $stmt->execute($params);
            return $stmt->fetchAll();       
        }catch(PDOException $e)
        {
            throw new Exception($e->getMessage());
        }       
    }
    
    /**
     * Método para inserir um registro no banco de dados
 	 * @param STRING $query - SQL insert
	 * @param STRING $params - Parâmetros do SQL
     */
    public function insertRow($query, $params)
    {
        try
        { 
            $stmt = $this->objDb->prepare($query); 
            $stmt->execute($params);
        }catch(PDOException $e)
        {
            throw new Exception($e->getMessage());
        }           
    }
    
    /**
     * Método para atualizar um registro no banco de dados
 	 * @param STRING $query - SQL update
	 * @param STRING $params - Parâmetros do SQL
     * @return [[Type]] [[Description]]
     */
    public function updateRow($query, $params)
    {
        return $this->insertRow($query, $params);
    }
    
    /**
     * Método para remover um registro
     * @param  STRING $query  - SQL script para deletar o registro
     * @param  ARRAY(STRING) $params - Chave do registro a ser deletado
     * @return [[Type]] [[Description]]
     */
    public function deleteRow($query, $params)
    {
        return $this->insertRow($query, $params);
    }
}


/***
 * USAGE
 * 
 * Conectando com o banco de dados
 * $db = new db("root", "suasenha", "localhost", "crud", array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
 * 
 * Recuperando um registro
 * $getrow = $db->getRow("SELECT firstname, lastname FROM users WHERE firstname =?", array("fname10"));
 * 
 * Recuperando múltiplos registros (várias linhas)
 * $getrows = $db->getRows("SELECT firstname, lastname FROM users");
 *  
 * Inserindo um registro
 * $insertrow = $db ->insertRow("INSERT INTO users (firstname, lastname) VALUES (?, ?)", array("Pedro", "Alvares Cabral"));
 *  
 * Atualizando um registro
 * $updaterow = $db->updateRow("UPDATE users SET firstname = ?, lastname = ? WHERE id = ?", array("Tira", "Dentes", "70"));
 *  
 * Apagando um registro
 * $deleterow = $db->deleteRow("DELETE FROM users WHERE id = ?", array("70"));
 * isconnecting from database
 * $db->Disconnect();
 * 
 * Verificando se o banco de dados esta conectado
 * if($db->isConnected)
 * {
 * 	echo "Conexão com banco de dados estabelecida.";
 * }else
 * {
 * 	echo "Conexão com banco de dados NÃO estabelecida.";
 * }
 **/