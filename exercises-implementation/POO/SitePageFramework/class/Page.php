<?php
/**
 * Classe para montagem da página do site
 * @createDate 04/05/2015
 * @author George Mendonça
 * Disciplina de Programação Orientada a Objetos (POO)
 */
class Page {
    private $tituloCab = null;
    private $charset= null;
    private $css = array();
    private $tituloSite = null;
    private $conteudo = null;
    private $txtRodape = null;
    
    /**
     * Método SET Pattern
     * @param STRING $atributo - Atributo da classe
     * @param STRING $valor - Valor do atributo da classe
     */
    public function __set($atributo, $valor) {
        $this->$atributo = $valor;
    }

    /**
     * Método GET Pattern
     * @param  STRING $atributo - Atributo da classe
     * @return - Atributo da classe
     */
    public function __get($atributo) {
        return $this->$atributo;
    }
}