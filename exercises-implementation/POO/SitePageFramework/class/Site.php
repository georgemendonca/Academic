<?php
/**
 * Classe para desenvolvimento do site
 * @createDate 04/05/2015
 * @author George Mendonça
 * Disciplina de Programação Orientada a Objetos (POO)
 */
class Site {
    
    /**
     * Método para montar o cabeçalho de uma página do site
     * @param Page $pg - Objeto da classe Pagina
     */
    public static function cabecalho(Page $pg) {
        ?>
        <!DOCTYPE html>
        <html lang="ptBR">
        <html>
            <head>
                <meta charset="<?php echo $pg->__get('charset'); ?>">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title><?php echo $pg->__get('tituloCab'); ?></title>
                <?php                
                $csss = $pg->__get('css');
                
                foreach($csss as $key => $css) {
                    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$css}\">";
                }
                ?>                
            </head>
        <body>

            <div id="header">
                <h1><?php echo $pg->__get('tituloSite'); ?></h1>
            </div>
        <?php
    }
    
    /**
     * Método para montar o corpo da página do site
     * @param Page $pg - Objeto da classe Pagina
     */
    public static function corpo(Page $pg) {
        ?>
        <div id="nav">
            Home<br>
            <a href="http://git-scm.com/book/pt-br/v1/" target="_blank">Git</a><br>
            <a href="http://php.net/" target="_blank">PHP.net</a>
        </div>

        <div id="section">
            <h1>Objetos para construção de uma página web</h1>
            <p>
                Neste exemplo são demonstrados objetos para construção de uma simples página, onde as informações são passadas por argumento.
            </p>
            <p>
                Estrutura:<br />
                - Pagina<br />
                -- class<br />
                ---Db.php<br />
				---Page.php<br />
				---Site.php<br />
                -- css<br />
                --- style.css<br />
                -- index_exemplo.html<br />
                -- index.php
            </p>
        </div>
        <?php
    }
    
    /**
     * Método para montar o rodapé da página do site
     * @param Page $pg - Objeto da classe Pagina
     */
    public static function rodape(Page $pg) {
        ?>
            <div id="footer">
                <?php echo $pg->__get('txtRodape'); ?>
            </div>

        </body>

        </html>
        <?php
    }
}