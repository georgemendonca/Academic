<?php
/**
 * Classe para desenvolvimento do site
 * @createDate 04/05/2015
 * @author George Mendonça
 * Disciplina de Programação Orientada a Objetos (POO)
 */
class Site {
    public static function cabecalho($cfgCab = array()) {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="<?php echo $cfgCab['charset']; ?>">
                <title><?php echo $cfgCab['title']; ?></title>
                <?php
                $csss = $cfgCab["css"];
                foreach($csss as $key => $css) {
                    echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$css}\">";
                }
                ?>                
            </head>
        <body>

            <div id="header">
                <h1><?php echo $cfgCab['titleSite']; ?></h1>
            </div>
        <?php
    }
    
    public static function corpo() {
        ?>
        <div id="nav">
            Home<br>
            <a href="http://git-scm.com/book/pt-br/v1/" target="_blank">Git</a><br>
            <a href="http://php.net/" target="_blank">PHP.net</a>
        </div>

        <div id="section">
            <h1>Objetos para construção de uma página web</h1>
            <p>
                Neste exemplo são demonstrados objetos para construção de uma simples página, onde as informações são passadas por argumento.
            </p>
            <p>
                Estrutura:<br />
                - Pagina<br />
                -- class<br />
                ---Site.php<br />
                -- css<br />
                --- style.css<br />
                -- index_exemplo.html<br />
                -- index.php
            </p>
        </div>
        <?php
    }
    
    public static function rodape($txtRodape = "Copyright © W3Schools.com") {
        ?>
            <div id="footer">
                <?php echo $txtRodape; ?>                
            </div>

        </body>

        </html>
        <?php
    }
}
?>