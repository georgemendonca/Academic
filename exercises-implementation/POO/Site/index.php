<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include "class/Site.php";

// Configuração do Cabeçalho
$configCab = array(
    'title'       => 'Site dinâmico em JAVA',
    'charset'     => 'UTF-8',
    'css'         => array('css/style.css'),
    'titleSite'   => 'Dinamizando um template HTML do com o 4° Semestre'
);
// Configuração do Rodapé
$titleRodape ='Copyright © 4° Semestre TADS';

// Montando a página
Site::cabecalho($configCab);
Site::corpo();
Site::rodape($titleRodape);