<?php
class Page {
    public static function cabecalho($title, $charset, $tileSite) {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="<?php echo $charset; ?>">
                <title><?php echo $title; ?></title>
                <link rel="stylesheet" type="text/css" href="css/style.css">
            </head>
        <body>

            <div id="header">
                <h1><?php echo $tileSite; ?></h1>
            </div>
        <?php
    }
    
    public static function corpo() {
        ?>
        <div id="nav">
            Home<br>
            <a href="http://git-scm.com/book/pt-br/v1/" target="_blank">Git</a><br>
            <a href="http://php.net/" target="_blank">PHP.net</a>
        </div>

        <div id="section">
            <h1>Objetos para construção de uma página web</h1>
            <p>
                Neste exemplo são demonstrados objetos para construção de uma simples página, onde as informações são passadas por argumento.
            </p>
            <p>
                Estrutura:<br />
                - Pagina<br />
                -- class<br />
                ---Pagina.php<br />
                -- css<br />
                --- style.css<br />
                -- index_exemplo.html<br />
                -- index.php
            </p>
        </div>
        <?php
    }
    
    
    public static function rodape($txtRodape = "Copyright © W3Schools.com") {
        ?>
            <div id="footer">
                <?php echo $txtRodape; ?>
            </div>

        </body>

        </html>
        <?php
    }
}